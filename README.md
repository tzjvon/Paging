### Paging

#### usage
```js
new Paging(el, opts)
```
'el' 是Element或者class或者id，也可以不传入这个参数
* 'opts' 是一个object，里面有三个参数
	* 'limitDistance': 距离底部多少距离时开始分页
	* 'paging' ：分页的时触发的函数


example
```
new Paging( "#container",{
	paging: function () {
		this.isPaging = true
		var that = this

		setTimeout(function  () {
			console.log("Paging")
			that.isPaging = false
		}, 1000)
	}
})
```
