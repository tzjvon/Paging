const util = require("./../lib/util")

let core = require("./../lib/core")

/*
* 默认是body 只有一个object参数
*
*/
function Paging() {
	var args = arguments,
		len = args.length,
		el, opts
	switch (len) {
		case 1: {
			opts = args[0]
			this.container = document
			this.wrapper = document.body
		}
			break;
		case 2: {
			el = args[0]
			opts = args[1]
			if (util.isDom(el)) {
				this.container = el
			}else {
				this.container = document.querySelector(el)
			}

			this.wrapper = this.container.children[0]
		}
			break;
	}


	this.opts = {}
	this.opts["limitDistance"] = opts["limitDistance"] || 90
	this.opts["paging"] = opts["paging"] || util.noop

	this.isPaging = false

	if (util.type(opts) === "object") {
		util.extends(this.opts, opts)
	}


	this.init()
	this._bindEvent()

}

util.extends(Paging.prototype, core)



module.exports = Paging

