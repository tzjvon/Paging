const util = require("./util")

module.exports = {
	version: "0.0.1",

	init: function () {
		let container;
		if (this.container == document) {
			if (!this.container.tagName) {
				container = window
			}
		}else {
			container = this.container
		}

		if (container === window) {
			this.containerHeight = window.innerHeight || window.outerHeight
			this.wrapperHeight = document.body.offsetHeight
		}else {
			this.containerHeight = parseFloat(getComputedStyle(container)["height"]) || container.offsetHeight
			this.wrapperHeight =  parseFloat(getComputedStyle(this.wrapper)["height"]) || this.wrapper.offsetHeight
		}

	},

	handleEvent: function  (e) {
		var evt = e || window.event
		switch (evt.type) {
			case "scroll":
				this._scroll(evt)
				break;
		}
	},

	_scroll: function (e) {
		if (this.isPaging) {return;}
		let target = e.currentTarget,
			scrollTop = target.scrollTop || document.documentElement.scrollTop,
			leftDistance = this.wrapperHeight - scrollTop -this.containerHeight

		if (leftDistance <= this.opts.limitDistance) {
			this.opts.paging.call(this)
		}
	},
	_bindEvent: function () {
		if (this.container === document) {
			util.addEvent(window, "scroll", this, false)
		}else {
			util.addEvent(this.container, "scroll", this, false)
		}
	}

}
