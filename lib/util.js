module.exports = {
	noop: function () {},
	addEvent: function (el, type, fn, capture) {
		el.addEventListener(type, fn, !!capture || false)
	},
	removeEvent: function (el, type, fn) {el.removeEventListener(type, fn) },

	extends: function (target, obj, deep) {
		for (var key in obj) {
			if (deep) {
				target[key] = obj[key]
			}else {
				if (Object.prototype.hasOwnProperty.call(obj, key)) {
					target[key] = obj[key]
				}else {
					continue;
				}
			}
		}
	},
	isDom: function (obj) {
		try {
			return obj instanceof HTMLElement
		} catch(e) {
			return (typeof obj === "object") &&
			(obj.nodeType ===1) && (typeof obj.style === "object") &&
			(typeof obj.ownerDocument === "object")
		}
	},
	type: function (obj) {
		return Object.prototype.toString.call(obj).replace(/\[object\s|\]/g, "")
	}

}
